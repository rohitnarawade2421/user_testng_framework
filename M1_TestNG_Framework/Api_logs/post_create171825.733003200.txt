endpoint is :
https://reqres.in/api/users

request body is:
{
    "email": "pekka@yahoo.com",
    "password": "pekka07"
}

response header date is :
Mon, 04 Mar 2024 11:48:21 GMT

response body is :
{
    "email": "pekka@yahoo.com",
    "password": "pekka07",
    "id": "609",
    "createdAt": "2024-03-04T11:48:21.872Z"
}

