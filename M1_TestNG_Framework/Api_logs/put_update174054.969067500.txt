endpoint is :
https://reqres.in/api/users/2

request body is:
{
    "name": "morpheus",
    "job": "leader"
}

response header date is :
Fri, 08 Mar 2024 12:10:50 GMT

response body is :
{
    "name": "morpheus",
    "job": "leader",
    "updatedAt": "2024-03-08T12:10:50.445Z"
}

