endpoint is :
https://reqres.in/api/users

request body is:
{
    "name": "morpheus",
    "job": "leader"
}

response header date is :
Fri, 08 Mar 2024 12:10:54 GMT

response body is :
{
    "name": "morpheus",
    "job": "leader",
    "id": "896",
    "createdAt": "2024-03-08T12:10:53.960Z"
}

