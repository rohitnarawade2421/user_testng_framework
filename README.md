# User_TestNG_Framework
This repository contains a TestNG-based automation framework
Created RestAssured TestNG framework using java programming language encapsulating all our requirements:


configure all Rest APIs
 > Extract response = used rest Assured library

 > Parse the response = used jsonpath class which belongs to rest assured library .

 > Validate the response = used testNG library.

framework is capable of reusability by creating common methods and centralised configuration files

Framework is able to do data driven testing
>1.same class 

>2.different class


Annotations used:
>@test
>@beforetest
>@aftertest
>@dataprovider

Maven dependencies
> rest assured

> testNG

> ApachePOI-a.poi

> poi-ooxml

> poi-scratchpad

> poi-excelant

> poi-examples

> Allure-TestNG

> ExtentReports


Generated Allure report and Extent report.
